var LocalStrategy = require('passport-local').Strategy;

var User=require('./db/User');
module.exports=function(passport){
    passport.serializeUser(function(user, done) {
       
        done(null,user);
     })
      
     passport.deserializeUser(function(user, done) {
        
        done(null,user);
     })
     
     passport.use('local',new LocalStrategy(function(username,password,done){
       
        User.findOne({username:username},function(err,doc){
            if(err){
                console.log("error1");
                done(err)
            }
            else
            {
                if(doc){
                   var valid= doc.comparePassword(password,doc.password)
                   if(valid){
                        done(null,{
                            username:doc.username,
                            password:doc.password,
                        })
                   }else{
                    console.log("error2");
                        done(null,false);   
                    }
                }else{
                    console.log("error3");
                    done(null,false);
                }
            }
        })

     }));
}